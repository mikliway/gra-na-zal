package com.banaszak.tuneguitar;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends Activity {

    // Global parm
    public static Integer goBackCounter = 0;
    public Context mContext;
    private MediaPlayer musicFile;
    private AudioManager audio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //activ content
        mContext = MainActivity.this;

        //initialize audio sound volume +/-
        audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        //main page
        showMainScreen();
    }

    //main page
    private void showMainScreen(){
        setContentView(R.layout.main);
        goBackCounter += 1;
    }

    //menu page
    private void showMenuPage(){
        setContentView(R.layout.menu);
        goBackCounter = 0;

    }


    //bass page
    private void showBassPage(){
        setContentView(R.layout.bass);
        goBackCounter = 1;

    }

    //guitar page
    private void showGuitarPage(){
        setContentView(R.layout.guitar);
        goBackCounter = 1;
    }

    //onClick go to page
    public void clickImage(View view){
    int id = view.getId();

        //relase sound
        releaseSound();

        if (id == R.id.imageView){
            showMenuPage();

        } else if (id == R.id.guitar){
            showGuitarPage();

        } else if (id == R.id.bass){
            showBassPage();

        } else if (id == R.id.basse){
            playSound("bass/Ebass.mp3", 0);

        } else if (id == R.id.bassa){
            playSound("bass/Abass.mp3", 0);

        } else if (id == R.id.bassd){
            playSound("bass/Dbass.mp3", 0);

        } else if (id == R.id.bassg){
            playSound("bass/Gbass.mp3", 0);

        }else if (id == R.id.e){
            playSound("guitar/E.mp3", 0);

        } else if (id == R.id.a){
            playSound("guitar/A.mp3", 0);

        } else if (id == R.id.d){
            playSound("guitar/D.mp3", 0);

        } else if (id == R.id.g){
            playSound("guitar/G.mp3", 0);

        } else if (id == R.id.b){
            playSound("guitar/B.mp3", 0);

        } else if (id == R.id.ee){
            playSound("guitar/emale.mp3", 0);
        }

    }

    /*
 Play sounds from assests folder
 looping = 1 loop sound / 0 = do not loop sound
*/
    @SuppressWarnings({"SameParameterValue", "deprecation"})
    private void playSound(String fileName, int looping) {
        if ((fileName != null) && (!fileName.equals("-1"))) {
            AssetFileDescriptor afd = null;
            try {
                afd = getAssets().openFd(fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if ((afd != null) && (afd.getLength() > 0) && (afd.getStartOffset() > 0)) {
                releaseSound();
                musicFile = new MediaPlayer();
                try {
                    long start;
                    long end;
                    start = afd.getStartOffset();
                    end = afd.getLength();
                    String fileCheckMP;
                    fileCheckMP = afd.getFileDescriptor().toString();
                    if (fileCheckMP != null) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                                    .setUsage(AudioAttributes.USAGE_GAME)
                                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                                    .build();
                            musicFile.setAudioAttributes(audioAttributes);
                        } else {
                            musicFile.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        }
                        if (musicFile != null) {
                            musicFile.reset();
                        }
                        musicFile.setDataSource(afd.getFileDescriptor(), start, end);
                        afd.close();
                        try {
                            if (musicFile != null) {
                                musicFile.prepare();
                                if (looping == 1) {
                                    musicFile.setLooping(true);
                                } else {
                                    musicFile.setLooping(false);
                                }
                                if (musicFile.getDuration() > 0) {
                                    musicFile.start();
                                    musicFile.setVolume(3, 3);
                                }
                            }
                        } catch (IllegalStateException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                } catch(IllegalArgumentException | IllegalStateException | IOException e){
                    e.printStackTrace();
                }
            }
        }
    }

    //Release sound from assests folder
    private void releaseSound() {
        if (musicFile != null) {
            musicFile.release();
            musicFile = null;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        releaseSound();
    }


    //When clicking on the back key in the phone/tablet
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && (goBackCounter > 2)) {
            finish();
            System.exit(0);
        }  else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
            return true;
        } else {
            if (goBackCounter == 1){
                showMenuPage();
            } else {
                goBackCounter += 2;
                showMainScreen();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    //When clicking on the back key in the phone show message
    @Override
    public void onBackPressed() {
        releaseSound();
        if (goBackCounter >= 2) {
            if (!((Activity) mContext).isFinishing()) {
                Toast.makeText(mContext, getResources().getString(R.string.exit), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
